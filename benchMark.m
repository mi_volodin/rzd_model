%% Test

%% SrcData

distance_ij = ...
[27	8	1109;
27	9	1217;
51	8	991;
51	9	822;
];

fun_param_list = ...
[2	8	0	0	5602.09;
2	9	0	0	7337.48;
1	27	-0.0394	4764.2946	0;
1	51	-0.0014	3965.7909	0;
];

region_code = ...
{ 8	'������� �������';
9	'�������� �������';
27	'��������� �������';
51	'������������� �������'};


%% InitCode

% !TMP: C_ij init
c_ij_matrix = distance_ij;
for i = 1:size(c_ij_matrix,1)
    if distance_ij(i,3) < 2000
        c_ij_matrix(i,3) = (50.63 * distance_ij(i,3) + 18710.85) / 64;
    else
        c_ij_matrix(i,3) = (20.46 * distance_ij(i,3) + 79197.05) / 64;
    end
end
% adjust price 64 tonns


c_ij_matrix = sortrows(c_ij_matrix, [1 2]);
    

% Optim: 1/2 * <Hx,x> + <f,x>; subj to A * x <= b
% x = [[ X, Y, Z]]

% split cons and prod
prod_param_list = fun_param_list(fun_param_list(:,1) == 2, [2 5]);
cons_param_list = fun_param_list(fun_param_list(:,1) == 1, [2 3 4]);

% init dimensions
cons_amount = size(cons_param_list, 1);
prod_amount = size(prod_param_list, 1);
link_amount = size(c_ij_matrix, 1);

x_dim = prod_amount + cons_amount + link_amount;
%x_row = zeros(1, x_dim);
% init H
H = zeros(x_dim);

for i = 1 : cons_amount % !speed (diag init)
    H(i,i) = cons_param_list(i, 2); 
end

% init f
% f = cons(i,3) + prod(i, 2) + c_ij_matrix(i, 3)

f = zeros(x_dim, 1);
for c = 1:cons_amount;
    f(c) = cons_param_list(c, 3);
end

for p = 1:prod_amount;
    f(p) = - prod_param_list(p, 2);
end

for l = 1:link_amount;
    f(l) = - c_ij_matrix(l, 3);
end

% init A
% A = {1, if X; -1 if Y; sum if z}
% b = 0

A = zeros(cons_amount + prod_amount, x_dim);
b = zeros(cons_amount + prod_amount, 1);
index = 1:link_amount;
for c = 1 : cons_amount
    A(c,c) = 1;
    
    region_id = cons_param_list(c, 1);
    
    c_ij_rows_mask = (c_ij_matrix(:, 1) == region_id);
    c_ij_rows_ind = index(c_ij_rows_mask);
    
    A(c, cons_amount + prod_amount + c_ij_rows_ind) = -1;
end

for p = cons_amount + 1 : cons_amount + prod_amount
    A( p, p) = -1;
    
    region_id = prod_param_list(p - cons_amount, 1);
    
    c_ij_rows_mask = (c_ij_matrix(:, 2) == region_id);
    c_ij_rows_ind = index(c_ij_rows_mask);
    
    A(p, cons_amount + prod_amount + c_ij_rows_ind) = 1;
end

% init lb
lb = zeros(x_dim, 1);
%ub = inf(x_dim, 1);
%
%% Solve quadprog
opts = optimset('Algorithm', 'interior-point-convex');
[x, fval, eflag, ~, lambda] = quadprog(- H * 2, - f, A, b, [], [], lb, [], [], opts);

fun = @(x) x' * (- 2 * H) * x - f' * x;
