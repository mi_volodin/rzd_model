function [ obj ] = InitFromStandardizedExcel( obj, excelFilename, sheetList )
%INITFROMSTANDARDIZEDEXCEL Loads Raw data from excel file with fixed data
%storage scheme
    assert(nargin == 3, 'Not enough or too much arguments');
    assert(iscell(sheetList) == 1, 'SheetList must be cell array of strings');
    for i = 1:numel(sheetList)
        lname = sheetList{i};
        obj.RawData.(lname) = dataset('XLSFile', excelFilename, 'Sheet', lname);
        mask = isnan(double(obj.RawData.(lname)(:,1)));
        obj.RawData.(lname)(mask,:) = [];       
    end
    
    obj.InitModelDataFromRawData();
end

