function [ obj ] = ParseAndJoinResultsToRaw( obj )
%PARSEANDJOINRESULTSTORAW Summary of this function goes here
%   Detailed explanation goes here
    
%% create results dataset
obj.Results.fun_param_list = obj.RawData.fun_param_list;
obj.Results.fun_param_list.xy_val =  ... 
    obj.OptResults.x(obj.ModelData.Dims.x(1) : ...
                     obj.ModelData.Dims.y(2));
obj.Results.c_ij_matrix = obj.RawData.c_ij_matrix;
obj.Results.c_ij_matrix.z_val = obj.OptResults.x( obj.ModelData.Dims.z(1) : ...
                                                  obj.ModelData.Dims.z(2));

obj.Results.fun_param_list.lambda_k = obj.OptResults.lambda.ineqlin;
assert(obj.ModelData.Dims.y(2) == numel(obj.OptResults.lambda.ineqlin), ...
    'ParseAndJoin: Wrong lambda dimension');
                                              
end

