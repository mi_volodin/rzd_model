function [ obj ] = SolveOptimizationProblem( obj )
%SOLVEOPTIMIZATIONPROBLEM Summary of this function goes here
%   Detailed explanation goes here

opts = optimset('Algorithm', 'interior-point-convex');
[x, fval, eflag, ~, lambda] = quadprog(- obj.ModelData.H * 2, ...
                                       - obj.ModelData.f, ...
                                         obj.ModelData.A, ...
                                         obj.ModelData.b, ...
                                         [], [], ...
                                         obj.ModelData.lb, [], [], opts);
                                     
obj.OptResults.x = x;
obj.OptResults.fval = fval;
obj.OptResults.eflag = eflag;
obj.OptResults.lambda = lambda;
end

