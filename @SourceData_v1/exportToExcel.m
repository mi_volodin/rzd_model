function exportToExcel( obj, filename )
%EXPORTTOEXCEL Summary of this function goes here
%   Detailed explanation goes here
    warning('off','MATLAB:xlswrite:AddSheet');
    export(obj.Results.fun_param_list,'XLSfile',filename, 'Sheet', 'fun_param_list');
    export(obj.Results.c_ij_matrix,'XLSfile',filename, 'Sheet', 'c_ij_matrix');
    export(obj.RawData.region_code,'XLSfile',filename, 'Sheet', 'region_code');
    warning('on','MATLAB:xlswrite:AddSheet');
end

