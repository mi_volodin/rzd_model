function [ obj ] = InitModelDataFromRawData( obj )
%INITMODELDATAFROMRAWDATA Summary of this function goes here
%   Detailed explanation goes here

%% InitCode

% get region_codes
%all_region_codes = unique(obj.RawData.fun_param_list.region_code);
cons_region_codes = unique(obj.RawData.fun_param_list( ...
                           obj.RawData.fun_param_list.region_type == 1, ...
                           {'region_code'}));
prod_region_codes = unique(obj.RawData.fun_param_list( ...
                           obj.RawData.fun_param_list.region_type == 2, ...
                           {'region_code'}));
%all_region_codes = cat(cons_region_codes, prod_region_codes);

% !TMP: C_ij init
c_ij_matrix = obj.RawData.distance_ij;
c_ij_matrix.Properties.VarNames{3} = 'c_ij';
mark_delete = ones(size(c_ij_matrix, 1), 1);
for i = 1:size(c_ij_matrix,1)
    curdist = obj.RawData.distance_ij.distance_ij(i);
    if curdist < 2000
        % adjust price 64 tonns
       c_ij_matrix.c_ij(i) = (50.63 * curdist + 18710.85) / 64;
    else
        % adjust price 64 tonns
       c_ij_matrix.c_ij(i) = (20.46 * curdist + 79197.05) / 64;
    end
    if any(c_ij_matrix.i_region_code(i) == cons_region_codes.region_code) && ...
       any(c_ij_matrix.j_region_code(i) == prod_region_codes.region_code)
       mark_delete(i) = 0;
    end
end



c_ij_matrix = c_ij_matrix(mark_delete == 0, :);
c_ij_matrix = sortrows(c_ij_matrix, {'i_region_code', 'j_region_code'});
obj.RawData.c_ij_matrix = c_ij_matrix;
%symmetrize
%!NOTE: We can avoid symmetric variables by explicitly assuming c_ij=c_ji,
%however z_ij ~= z_ji, so it will be easier to initialize problem with full
%matrix
%Order is irrelevant, used for easier interpretation

%c_ij_matrix = cat(1, c_ij_matrix, c_ij_matrix(:, {'j_region_code', 'i_region_code', 'c_ij'}));
%c_ij_matrix = sortrows(c_ij_matrix, {'i_region_code', 'j_region_code'});
    

% Optim: 1/2 * <Hx,x> + <f,x>; subj to A * x <= b
% x = [[ X, Y, Z]]

% split cons and prod
obj.RawData.fun_param_list = sortrows(obj.RawData.fun_param_list, {'region_type', 'region_code'});
prod_param_list = double(obj.RawData.fun_param_list( ...
        obj.RawData.fun_param_list.region_type == 2, ...
        {'region_code', 'p_j'}));
cons_param_list = double(obj.RawData.fun_param_list( ...
        obj.RawData.fun_param_list.region_type == 1, ...
        {'region_code', 'A', 'B'}));
    

% init dimensions
cons_amount = size(cons_param_list, 1);
prod_amount = size(prod_param_list, 1);
link_amount = size(c_ij_matrix, 1);

x_dim = prod_amount + cons_amount + link_amount;

obj.ModelData.Dims = dataset();
obj.ModelData.Dims.x = [1 ; cons_amount];
obj.ModelData.Dims.y = [1 + cons_amount; cons_amount + prod_amount];
obj.ModelData.Dims.z = [1 + cons_amount + prod_amount; x_dim];

% init H
H = zeros(x_dim);

for i = 1 : cons_amount % !speed (diag init)
    H(i,i) = cons_param_list(i, 2); 
end

% init f
% f = cons(i,3) + prod(i, 2) + c_ij_matrix(i, 3)

f = zeros(x_dim, 1);
for c = 1:cons_amount;
    f(c) = cons_param_list(c, 3);
end

for p = 1:prod_amount;
    f(p) = - prod_param_list(p, 2);
end

for l = 1:link_amount;
    f(l) = - c_ij_matrix.c_ij(l);
end

% init A
% A = {1, if X; -1 if Y; sum if z}
% b = 0

A = zeros(cons_amount + prod_amount, x_dim);
b = zeros(cons_amount + prod_amount, 1);
index = 1:link_amount;

for c = 1 : cons_amount
    A(c,c) = 1;
    
    region_id = cons_param_list(c, 1);
    
    c_ij_rows_mask = (c_ij_matrix.i_region_code == region_id);
    c_ij_rows_ind = index(c_ij_rows_mask);
    
    A(c, cons_amount + prod_amount + c_ij_rows_ind) = -1;
end

for p = cons_amount + 1 : cons_amount + prod_amount
    A( p, p) = -1;
    
    region_id = prod_param_list(p - cons_amount, 1);
    
    c_ij_rows_mask = (c_ij_matrix.j_region_code == region_id);
    c_ij_rows_ind = index(c_ij_rows_mask);
    
    A(p, cons_amount + prod_amount + c_ij_rows_ind) = 1;
end

% init lb
lb = zeros(x_dim, 1);
%ub = inf(x_dim, 1);
%

%% SAVE
obj.ModelData.H = H;
obj.ModelData.f = f;
obj.ModelData.A = A;
obj.ModelData.b = b;
obj.ModelData.lb = lb;
obj.ModelData.ub = [];
