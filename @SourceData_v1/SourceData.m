classdef SourceData < handle
    %SOURCEDATA Class for source information with interfaces
    
    properties
        RawData;
        ModelData;
        OptResults;
        Results;
    end
    
    methods
        function obj = SourceData()
            
        end
        
        InitFromStandardizedExcel(obj, excelFile, sheetList);
        exportToExcel(obj, filename);
        
    end
    
    methods (Access = public)
        InitModelDataFromRawData(obj);
        SolveOptimizationProblem(obj);
        ParseAndJoinResultsToRaw(obj);
    end
end

