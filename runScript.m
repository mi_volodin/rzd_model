%% Init section
% Initializing source data and model

sourceData = SourceData();
sheetList = {'fun_param_list', 'distance_ij', 'region_code','z_ij_matrix'};

excelFilename = 'SourceDataExcel_2016_03_31.xlsx';


sourceData.InitFromStandardizedExcel(excelFilename, sheetList, 1);
sourceData.SolveOptimizationProblem();
sourceData.ParseAndJoinResultsToRaw();
sourceData.exportToExcel('ResultsDataExcel_2016_03_31.xlsx');
