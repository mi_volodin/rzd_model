function [ obj ] = CalculateQualityFunctionals( obj )
%PARSEANDJOINRESULTSTORAW Summary of this function goes here
%   Detailed explanation goes here
    
%% create results dataset
obj.Results.qualityFuns = dataset( );



obj.Results.fun_param_list.lambda_k = obj.OptResults.lambda.ineqlin;
assert(obj.ModelData.Dims.y(2) == numel(obj.OptResults.lambda.ineqlin), ...
    'ParseAndJoin: Wrong lambda dimension');
                                              
end

function [res] = MaxAbsoluteDivergence(obj)
    
end
