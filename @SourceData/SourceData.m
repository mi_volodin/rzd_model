classdef SourceData < handle
    %SOURCEDATA Class for source information with interfaces
    
    properties
        RawData;
        ModelData;
        OptResults;
        Results;
    end
    
    methods
        function obj = SourceData()
            
        end
        
        InitFromStandardizedExcel(obj, excelFile, sheetList, use_reg_prices);
        exportToExcel(obj, filename);
        
    end
    
    methods (Access = public)
        InitModelDataFromRawData(obj, use_reg_prices);
        SolveOptimizationProblem(obj);
        ParseAndJoinResultsToRaw(obj);
    end
end

