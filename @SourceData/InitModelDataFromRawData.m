function [ obj ] = InitModelDataFromRawData( obj, use_reg_prices )
%INITMODELDATAFROMRAWDATA Summary of this function goes here
%   Detailed explanation goes here

%% InitCode

obj.RawData.fun_param_list = sortrows(obj.RawData.fun_param_list, {'region_type', 'region_code'});
% get region_codes
%all_region_codes = unique(obj.RawData.fun_param_list.region_code);
cons_region_codes = unique(obj.RawData.fun_param_list( ...
                           obj.RawData.fun_param_list.region_type == 1, ...
                           {'region_code'}));
prod_region_codes = unique(obj.RawData.fun_param_list( ...
                           obj.RawData.fun_param_list.region_type == 2, ...
                           {'region_code'}));
exp_region_codes = unique(obj.RawData.fun_param_list( ...
                           obj.RawData.fun_param_list.region_type == 3, ...
                           {'region_code'}));
%all_region_codes = cat(cons_region_codes, prod_region_codes);


%% calc reserves
%reserves_supply = calc_reserves_supply(obj,  99);
% save reserves in raw
obj.RawData.fun_param_list.reserves_supply = zeros(size(obj.RawData.fun_param_list, 1),1);
% for r = 1 : size(reserves_supply, 1)
%     obj.RawData.fun_param_list.reserves_supply(obj.RawData.fun_param_list.region_code == reserves_supply(r, 1)) = reserves_supply(r, 2);
% end
% obj.RawData.fun_param_list.reserves_supply(obj.RawData.fun_param_list.region_type == 1) = max(0, obj.RawData.fun_param_list.reserves_supply(obj.RawData.fun_param_list.region_type == 1));
% obj.RawData.fun_param_list.reserves_supply(obj.RawData.fun_param_list.region_type == 2) = min(0, obj.RawData.fun_param_list.reserves_supply(obj.RawData.fun_param_list.region_type == 2));

%% calc c_ij matrix
% !TMP: C_ij init
c_ij_matrix_reg = calc_c_ij_matrix(obj, cons_region_codes, prod_region_codes, exp_region_codes);
%symmetrize
%!NOTE: We can avoid symmetric variables by explicitly assuming c_ij=c_ji,
%however z_ij ~= z_ji, so it will be easier to initialize problem with full
%matrix    

%% calc real prices matrix
    z_ij_matrix = calc_z_ij_matrix(obj); 
    %temporary clean all except prod->cons regions
        del1 = setdiff(z_ij_matrix.i_region_code, prod_region_codes.region_code);
        for i = 1:size(del1, 1)
            z_ij_matrix(z_ij_matrix.i_region_code == del1(i),:) = [];
        end
        del2 = setdiff(z_ij_matrix.j_region_code, vertcat(cons_region_codes.region_code, exp_region_codes.region_code));
        for i = 1:size(del2, 1)
            z_ij_matrix(z_ij_matrix.j_region_code == del2(i),:) = [];
        end
        
        %join with regbased tariffs
        c_ij_matrix = join(z_ij_matrix, c_ij_matrix_reg, ...
                                    'Type', 'leftouter',    ...
                                    'Keys', {'i_region_code', 'j_region_code'}, ...
                                    'MergeKeys', true);
       
       c_ij_matrix.c_ij_reg(c_ij_matrix.j_region_code - c_ij_matrix.i_region_code == 100) = 0;
       %copy c_ij to c_ij_real
       c_ij_matrix.c_ij_real = c_ij_matrix.c_ij;
       if use_reg_prices > 0 
           c_ij_matrix.c_ij = c_ij_matrix.c_ij_reg;
       end
       obj.RawData.z_ij_matrix = c_ij_matrix;
       c_ij_matrix = c_ij_matrix(:,{'i_region_code','j_region_code','c_ij'});
    %% join z_ij to c_ij ... but currently unused
%     c_ij_matrix = join(c_ij_matrix, z_ij_matrix(:,{'i','j','c_ij_real'}), ...
%                     'Type','leftouter', ...
%                     'LeftKeys',{'i_region_code', 'j_region_code'}, ...
%                     'RightKeys',{'i','j'});
%     c_ij_matrix(:, {'i','j'}) = [];
%     c_ij_matrix.c_ij_real(isnan(c_ij_matrix.c_ij_real)) = inf();

%% Calc real import and export for locked regions

real_import = grpstats(z_ij_matrix(:, {'j_region_code', 'z_ij'}), 'j_region_code','sum');
real_import.Properties.ObsNames = [];
real_import.GroupCount = [];
real_export = grpstats(z_ij_matrix(:, {'i_region_code', 'z_ij'}), 'i_region_code','sum');
real_export.Properties.ObsNames = [];
real_export.GroupCount = [];

obj.RawData.fun_param_list = ...
    join( obj.RawData.fun_param_list, real_import, ...
            'Type', 'leftouter', ...
            'LeftKeys', 'region_code', ...
            'RightKeys', 'j_region_code' );
obj.RawData.fun_param_list = ...
    join( obj.RawData.fun_param_list, real_export, ...
            'Type', 'leftouter', ...
            'LeftKeys', 'region_code', ...
            'RightKeys', 'i_region_code' );
obj.RawData.fun_param_list.j_region_code = [];
obj.RawData.fun_param_list.i_region_code = [];
obj.RawData.fun_param_list.Properties.VarNames(end-1:end) = ...
    {'fact_import', 'fact_export'};

% arrrgghh... couldn't find anything faster without internet
for i = 1: size(obj.RawData.fun_param_list,1)
    obj.RawData.fun_param_list.fact_import (i) = ...
        max(0,obj.RawData.fun_param_list.fact_import (i)); 
    obj.RawData.fun_param_list.fact_export (i) = ...
        max(0,obj.RawData.fun_param_list.fact_export (i)); 
end
     
% Optim: 1/2 * <Hx,x> + <f,x>; subj to A * x <= b
% x = [[ X, Y, Z]]

% split cons, prod, export and virtual prod
obj.RawData.fun_param_list = sortrows(obj.RawData.fun_param_list, {'region_type', 'region_code'});
cons_param_list = double(obj.RawData.fun_param_list( ...
        obj.RawData.fun_param_list.region_type == 1, ...
        {'region_code', 'C_2013', 'p_2013', 'p','a','b', 'reserves_supply', 'impex_is_locked', 'fact_import'}));
exp_param_list = double(obj.RawData.fun_param_list( ...
        obj.RawData.fun_param_list.region_type == 3, ...
        {'region_code','p','C_2013', 'reserves_supply','impex_is_locked'}));
   %virtual producers regions for consumer regions
virtual_prod_param_list = double(obj.RawData.fun_param_list( ...
        obj.RawData.fun_param_list.region_type == 1, ...
        {'region_code', 'p_j','xy_max', 'reserves_supply','impex_is_locked', 'fact_import'})); %no production (impex) lock, since inner production may vary
virtual_prod_param_list(:,1) = virtual_prod_param_list(:,1) + 1000;
virtual_prod_param_list(:,4) = 0;
virtual_prod_param_list(:,5) = 0;
virtual_prod_param_list(virtual_prod_param_list(:,2) <= 0 | isnan(virtual_prod_param_list(:,2)), ...
                        3) = 0; %if prices are nonpositive or not available - then no production
% real producers
real_prod_param_list = double(obj.RawData.fun_param_list( ...
        obj.RawData.fun_param_list.region_type == 2, ...
        {'region_code', 'p_j','xy_max', 'reserves_supply','impex_is_locked', 'fact_export'}));
    
prod_param_list = [real_prod_param_list; virtual_prod_param_list];

%save virtual producers in RAW
obj.RawData.fun_param_list = join(obj.RawData.fun_param_list, ...
                                                dataset({virtual_prod_param_list, 'region_code', 'p_j','C_2013', 'reserves_supply', 'impex_is_locked', 'fact_import'}), ...
                                                'Type', 'outer', 'MergeKeys', true);
obj.RawData.fun_param_list = sortrows(obj.RawData.fun_param_list,{'region_type','region_code'},'ascend');
obj.RawData.fun_param_list.region_type(isnan(obj.RawData.fun_param_list.region_type))=4;
%create virtual links between virtual producers and consumers
virtual_links = dataset();
virtual_links.i_region_code = virtual_prod_param_list(:,1);
virtual_links.j_region_code = cons_param_list(:,1);
virtual_links.c_ij = zeros(size(cons_param_list,1),1);


exp_links = dataset();
exp_links.i_region_code = exp_param_list(:,1) - 100;
exp_links.j_region_code = exp_param_list(:,1);
exp_links.c_ij = zeros(size(exp_param_list, 1),1);
exp_links(~ismember(exp_links.i_region_code, prod_param_list(:,1)),:) = [];



basic_c_ij_matrix = c_ij_matrix;
c_ij_matrix = [c_ij_matrix; virtual_links; exp_links];
c_ij_matrix = sortrows(c_ij_matrix, {'i_region_code', 'j_region_code'});
obj.RawData.c_ij_matrix = c_ij_matrix;

%% init dimensions
cons_amount = size(cons_param_list, 1);
prod_amount = size(prod_param_list, 1);
virtual_prod_amount = size(virtual_prod_param_list, 1);
real_prod_amount = size(real_prod_param_list, 1);
exp_amount = size(exp_param_list, 1);
link_amount = size(c_ij_matrix, 1);
cons_locks_amount = sum(any(cons_param_list(:,8) > 0));
prod_locks_amount = sum(any(prod_param_list(:,5) > 0));

x_dim =  exp_amount + cons_amount + prod_amount + link_amount;

obj.ModelData.Dims = dataset();
obj.ModelData.Dims.x = [1; cons_amount]; %consumers
obj.ModelData.Dims.y = [1 + cons_amount; cons_amount + prod_amount]; %producers
obj.ModelData.Dims.xe=[1 + cons_amount + prod_amount; cons_amount + exp_amount + prod_amount]; %export
obj.ModelData.Dims.z = [1 + cons_amount + prod_amount + exp_amount; x_dim]; %transport

obj.ModelData.Dims.yv = obj.ModelData.Dims.y;
obj.ModelData.Dims.yr = obj.ModelData.Dims.y;
obj.ModelData.Dims.yv(1) = obj.ModelData.Dims.yv(2) - virtual_prod_amount + 1;
obj.ModelData.Dims.yr(2) = obj.ModelData.Dims.yr(1) + real_prod_amount - 1;

%% init constraints
% init H
H = zeros(x_dim);

for i = 1 : cons_amount % !speed (diag init)
    % v1: H(i,i) = cons_param_list(i, 2); 
    % region_code C_2013 p_2013 p a b reserves_supply impex_is_locked
    % p_2013 / (-a * C_2013 * 2)
    p = i - 1 + obj.ModelData.Dims.x(1);
    H(p,p) = cons_param_list(i, 3) / ...
            ( - cons_param_list(i, 5) * ...
                cons_param_list(i, 2) * 2);
end

% init f
% f = cons(i,3) + prod(i, 2) + c_ij_matrix(i, 3)

f = zeros(x_dim, 1);
for c = 1:cons_amount;
    %v1: f(c) = cons_param_list(c, 3);
    %region_code C_2013 p_2013 p a b
    p = c - 1 + obj.ModelData.Dims.x(1);
    f(p) = cons_param_list(c, 6) / ...
           cons_param_list(c, 5) * ...
           cons_param_list(c, 3);
end

for e = 1:exp_amount;
    p = obj.ModelData.Dims.xe(1) - 1 + e;
    f(p) = exp_param_list(e, 2);
end

for p = 1:prod_amount;
    k = obj.ModelData.Dims.y(1) - 1 + p;
    f(k) = - prod_param_list(p, 2);
end

for l = 1:link_amount;
    p = obj.ModelData.Dims.z(1) - 1 + l;
    f(p) = - c_ij_matrix.c_ij(l);
end

% init A
% A = {1, if X; -1 if Y; sum if z}
% b = 0

A = zeros(cons_amount + prod_amount + exp_amount, x_dim);
b = zeros(cons_amount + prod_amount + exp_amount, 1);
Aeq = zeros(cons_locks_amount + prod_locks_amount, x_dim);
beq = zeros(cons_locks_amount + prod_locks_amount, 1);
index = 1:link_amount;
lock_eq_ptr = 0;

for c = 1 : cons_amount
    w = c + obj.ModelData.Dims.x(1) - 1;
    A(c,w) = 1; %x of consumer
    
    region_id = cons_param_list(c, 1);
    
    c_ij_rows_mask = (c_ij_matrix.j_region_code == region_id);
    c_ij_rows_ind = index(c_ij_rows_mask);
    
    % z of all exporting regions to current
    A(c, obj.ModelData.Dims.z(1) - 1 + c_ij_rows_ind) = -1;
    b(c) = cons_param_list(c, 7); %reserves supply in right part
    
    if cons_param_list(c, 8) > 0 %impex is locked
        
        c_ij_rows_mask_l = (basic_c_ij_matrix.j_region_code == region_id);
        c_ij_rows_ind_l = index(c_ij_rows_mask_l);
        lock_eq_ptr = lock_eq_ptr + 1;      
        Aeq(lock_eq_ptr, ...
            obj.ModelData.Dims.z(1) - 1 + c_ij_rows_ind_l) = 1;
        beq(lock_eq_ptr) = cons_param_list(c, 9);
    end
end

for p = 1 : prod_amount
    w1 =  p + cons_amount;
    w2 =  p + obj.ModelData.Dims.y(1) - 1;
    A( w1, w2) = -1;
    
    region_id = prod_param_list(p, 1);
    
    c_ij_rows_mask = (c_ij_matrix.i_region_code == region_id);
    c_ij_rows_ind = index(c_ij_rows_mask);
    
    A(w1, obj.ModelData.Dims.z(1) - 1 + c_ij_rows_ind) = 1;
    b(w1) = prod_param_list(p, 4);
end

for e = 1 : exp_amount
    w1 = e + cons_amount + prod_amount;
    w2 = e + obj.ModelData.Dims.xe(1) - 1;
    A(w1, w2) = 1;
    
    region_id = exp_param_list(e, 1);
    
    c_ij_rows_mask = (c_ij_matrix.j_region_code == region_id);
    c_ij_rows_ind = index(c_ij_rows_mask);
    
    A(w1, obj.ModelData.Dims.z(1) - 1 + c_ij_rows_ind) = -1;
    b(w1) = 0;
end


% init lb
lb = zeros(x_dim, 1);
ub = inf(x_dim, 1);
ub(obj.ModelData.Dims.xe(1) : obj.ModelData.Dims.xe(2)) = exp_param_list(:, 3);
ub(obj.ModelData.Dims.y(1) : obj.ModelData.Dims.y(2)) = prod_param_list(:, 3);



%

%% SAVE
obj.ModelData.H = H;
obj.ModelData.f = f;
obj.ModelData.A = A;
obj.ModelData.b = b;
obj.ModelData.Aeq = Aeq;
obj.ModelData.beq = beq;
obj.ModelData.lb = lb;
obj.ModelData.ub = ub;

function reserves_supply = calc_reserves_supply(obj,  reserve_region_code)
    z_ij_matrix = obj.RawData.z_ij_matrix;
    
    reserves_supply     = double(z_ij_matrix(z_ij_matrix.i_region_code == reserve_region_code, {'j_region_code', 'z_ij'}));
    reserves_withdraw = double(z_ij_matrix(z_ij_matrix.j_region_code == reserve_region_code, {'i_region_code', 'z_ij'}));
    
    reserves_withdraw(:, 2) = - reserves_withdraw(:, 2);
    
    reserves_supply = [reserves_supply; reserves_withdraw];
  
        
function c_ij_matrix = calc_c_ij_matrix(obj, cons_region_codes, prod_region_codes, exp_region_codes)

    c_ij_matrix = obj.RawData.distance_ij;
    c_ij_matrix.c_ij_reg = zeros(size(c_ij_matrix.distance_ij));
    
    c_ij_matrix = c_ij_matrix( ...
                       ismember(c_ij_matrix.i_region_code, prod_region_codes.region_code) & ...
                       ismember(c_ij_matrix.j_region_code, [cons_region_codes.region_code ; exp_region_codes.region_code]),:);
  
    dist = c_ij_matrix.distance_ij;
    c_ij_matrix.c_ij_reg = (dist <      2000) .* (50.63 * dist + 18710.85) / 64 + ...
                                    (dist >=    2000) .*(20.46 * dist + 79197.05) / 64;

    c_ij_matrix = sortrows(c_ij_matrix, {'i_region_code', 'j_region_code'});
    obj.RawData.c_ij_matrix = c_ij_matrix;

function z_ij_matrix = calc_z_ij_matrix(obj)
    % 11.04.2015 added 99 region as special region
    con_prices = obj.RawData.fun_param_list(obj.RawData.fun_param_list.region_type == 1, ...
                     {'region_code', 'p'});
    con_prices.Properties.VarNames(end) = {'p_d'};
    
    exp_prices = obj.RawData.fun_param_list(obj.RawData.fun_param_list.region_type == 3, ...
                    {'region_code', 'p'});
    exp_prices.Properties.VarNames(end) = {'p_d'};
    exp_prices.region_code = exp_prices.region_code - 100;
    
    prod_prices = obj.RawData.fun_param_list(obj.RawData.fun_param_list.region_type == 2, ...
                    {'region_code', 'p_j'});
    prod_prices.Properties.VarNames(end) = {'p_s'};
    
    z_ij_matrix = obj.RawData.z_ij_matrix;
    %drop empty 
    z_ij_matrix(z_ij_matrix.z_ij == 0, :) = [];
    
    %join prod_prices
    z_ij_matrix = join(z_ij_matrix, prod_prices, 'Type', 'leftouter', ...
                        'LeftKeys', 'i_region_code', 'RightKeys', 'region_code');
    z_ij_matrix.region_code = [];

    %join con_prices
    z_ij_matrix = join(z_ij_matrix, [con_prices; exp_prices], 'Type', 'leftouter', ...
                        'LeftKeys', 'j_region_code', 'RightKeys', 'region_code');           
    z_ij_matrix.region_code = [];
    
    %filter i for production only
    z_ij_matrix(~ismember(z_ij_matrix.i_region_code, prod_prices.region_code), :) = [];
    
    %filter 
    z_ij_matrix(~ismember(z_ij_matrix.j_region_code, [con_prices.region_code; exp_prices.region_code]), :) = [];
    z_ij_matrix.c_ij = z_ij_matrix.p_d - z_ij_matrix.p_s;
    z_ij_matrix(isnan(z_ij_matrix.c_ij), :) = [];
    z_ij_matrix(z_ij_matrix.c_ij <= 0, :) = [];
    
    %fix export
    exp_mask = ismember(z_ij_matrix.j_region_code, exp_prices.region_code);
    z_ij_matrix.j_region_code(exp_mask) = z_ij_matrix.j_region_code(exp_mask) + 100;
    
    %if there were exp + cons
    %???


