function [ x ] = adjustX( x, cons_i, prod_j, ModelData )
%ADJUSTX Summary of this function goes here
%   Detailed explanation goes here
    X_load = ModelData.Dims.x(1) + cons_i - 1;
    Y_load = ModelData.Dims.y(1) + prod_j - 1;
    Z_load = ModelData.Dims.z(1) + 1;
    x(X_load) =  x(X_load) + 1;
    x(Y_load) =  x(Y_load) + 1;
    x(Z_load) =  x(Z_load) + 1;

end

